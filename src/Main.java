
import br.com.unioeste.esi.hospital.control.Conexao;
import br.com.unioeste.esi.hospital.model.bo.ConsultaBO;
import br.com.unioeste.esi.hospital.model.vo.TipoConsulta;
import br.com.unioeste.esi.hospital.model.bo.TipoConsultaBO;
import br.com.unioeste.esi.hospital.model.vo.Consulta;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author rafakx
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Conexao.conectar();
        Consulta d;
        
        
        try { 
            String dataString = "15/08/2014";
            DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");  
            java.sql.Date data = new java.sql.Date(fmt.parse(dataString).getTime());
            java.sql.Date dataSql = new java.sql.Date(data.getTime());  
            java.sql.Time hrSql = new java.sql.Time(data.getTime()); 
        
            d = new Consulta(1,1,1,1,1,data,hrSql);
            
            ConsultaBO bo;
            bo = new ConsultaBO();
            System.out.println(bo.Create(d));

        } 
        
        
        catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
