package br.com.unioeste.esi.hospital.control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Conexao {
    public static Connection con;
    public static Statement stm;

    public static void conectar(){
        String url = "jdbc:mysql://localhost/hospital";
        String usr = "root";
        String pwd = "fuDcwCN5TAvVDxaW";


        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url,usr,pwd);
            stm = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 0);	
            System.out.println("Conectado");
        }
        catch(SQLException ex){
            System.out.println("SQLEXception "+ ex.getMessage());
            System.out.println("SQLState "+ ex.getSQLState());
            JOptionPane.showMessageDialog(null,"Não foi possível conectar ao banco! Encerrando programa.","Mensagem!",JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        catch(Exception e){
            System.out.println("Não foi possível conectar ao banco \n"+e.getMessage());
            JOptionPane.showMessageDialog(null,"Não foi possível conectar ao banco! Encerrando programa.","Mensagem!",JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }

    public static void fechar(){
        try {
        con.close();
        System.out.println("Conexão fechada");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}