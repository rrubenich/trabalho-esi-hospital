package br.com.unioeste.esi.hospital.model.bo;

import br.com.unioeste.esi.hospital.model.vo;
import br.com.unioeste.esi.hospital.model.dao;

import java.util.ArrayList;

public class MedicoBO {
	
    private static boolean verificar(Medico medico) {
    	
        return (medico.getNome() != null) &&
        		(medico.getCpf() != null) &&
        		(medico.getCrm() != null) &&
        		(medico.getIdEndereco() > 0);
        
    }
    
    public static void inserirMedico(Medico medico) {
    	
    	if (verificar(medico)) {
    		if (!MedicoDAO.isMedicoInserido(medico.getCpf())) {
    			MedicoDAO.inserirMedico(medico);
    		} else {
    			System.err.println("Médico já cadastrado");
    		}
    	} else {
    		System.err.println("Campos obrigatórios não preenchidos");
    	}
    	
    }
    
    public static void alterarMedico(Medico medico) {
    	
    	if (verificar(medico)) {
    		if (!MedicoDAO.isMedicoInserido(medico.getCpf())) {
    			MedicoDAO.alterarMedico(medico);
    		} else {
    			System.err.println("Médico já cadastrado");
    		}
    	} else {
    		System.err.println("Campos obrigatórios não preenchidos");
    	}
    	
    }
    
    public static Medico carregarMedico(int idMedico) {
    	
    	if (idMedico > 0) {
    		return MedicoDAO.carregarMedico(idMedico);
    	} else {
    		return null;
    	}
    	
    }

    public static int carregarIdMedico(String cpfMedico) {
    	if (cpfMedico != null) {
    		return MedicoDAO.carregarIdMedico(cpfMedico);
    	} else {
    		return 0;
    	}
    }    

    public static void removerMedico(int idMedico) {
    	
    	if (idMedico > 0) {
    		MedicoDAO.removerMedico(idMedico);
    	}
    	
    }
    
    public static ArrayList<Medico> ListarMedico() {
    	
        return MedicoDAO.listarMedico();
        
    }
    
}
