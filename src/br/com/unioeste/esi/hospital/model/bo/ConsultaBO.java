package br.com.unioeste.esi.hospital.model.bo;

import br.com.unioeste.esi.hospital.model.dao.ConsultaDAO;
import br.com.unioeste.esi.hospital.model.vo.Consulta;
import java.util.ArrayList;

public class ConsultaBO {
    
    ConsultaDAO coDAO = new ConsultaDAO();

    private boolean Verify(Consulta co){
        if((co.getIdMedico() > 0) || (co.getIdPaciente() > 0) || (co.getIdTipoConsulta() > 0) || (co.getIdHospital() > 0)){
            
            java.util.Date dataUtil = new java.util.Date();  
            java.sql.Date dataAtual = new java.sql.Date(dataUtil.getTime());
            
            //Verifica se a data é depois do dia atual.
            if(co.getDataConsulta().after(dataAtual)){
                
                //Verifica se não existe nenhuma consulta no horário marcado
                return coDAO.Read(co.getIdMedico(), co.getDataConsulta(), co.getHorarioConsulta()) == null;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public boolean Create(Consulta co) {
        if(Verify(co)){
            return coDAO.Create(co);
        }else{
            return false;
        }
    }

    public Consulta Read(int idConsulta) {
        if(idConsulta > 0){   
            return coDAO.Read(idConsulta);
        }else{
            return null;
        }
    }

    public boolean Update(Consulta co) {
        if(Verify(co)){
            return coDAO.Update(co);
        }else{
            return false;
        }
    }

    public boolean Delete(Consulta co) {
        if(co.getIdTipoConsulta() > 0){
            return coDAO.Delete(co);
        }else{
            return false;
        }
    }

    public ArrayList<Consulta> List() {
        return coDAO.List();
    }
    
}
