package br.com.unioeste.esi.hospital.model.bo;

import br.com.unioeste.esi.hospital.model.dao.ObservacaoDAO;
import br.com.unioeste.esi.hospital.model.vo.Observacao;
import java.util.ArrayList;

public class ObservacaoBO {
    
    ObservacaoDAO obDAO = new ObservacaoDAO();
    
    private boolean Verify(Observacao o){
         boolean ret = true;
         if(o.getIdConsulta() < 0){
             ret = false;
         }
         else if(o.getTituloObservacao().length() < 5){
             ret = false;
         }
         else if(o.getObservacao().length() < 10){
             ret = false;
         }
         return ret;
    }
     
     
    public boolean Create(Observacao ob) {
        if(Verify(ob)){
            return obDAO.Create(ob);
        }else{
            return false;
        }

    }

    public Observacao Read(int idObservacao) {
         if(idObservacao > 0){   
            return obDAO.Read(idObservacao);
        }else{
            return null;
        }
    }

    public boolean Update(Observacao ob) {
        if(Verify(ob)){
            return obDAO.Update(ob);
        }else{
            return false;
        }
    }

    public boolean Delete(Observacao ob) {
        if(ob.getIdObservacao() > 0){   
            return obDAO.Delete(ob);
        }else{
            return false;
        }
    }

    public ArrayList<Observacao> List(){
        return obDAO.List();
    }
}
