package br.com.unioeste.esi.hospital.model.bo;

import java.util.ArrayList;

import br.com.unioeste.esi.hospital.model.dao.HospitalDAO;
import br.com.unioeste.esi.hospital.model.vo.Hospital;

public class HospitalBO {
	
    private static boolean verificar(Hospital hospital) {
    	
        return (hospital.getNomeHospital() != null) &&
        		(hospital.getCnpjHospital() != null) &&
        		(hospital.getIdMedicoResponsavelHospital() > 0) &&
        		(hospital.getIdEnderecoHospital() > 0);
        
    }
    
    public static void inserirhospital(Hospital hospital) {
    	
    	if (verificar(hospital)) {
    		if (!HospitalDAO.isHospitalInserido(hospital.getCnpjHospital())) {
    			HospitalDAO.inserirHospital(hospital);
    		} else {
    			System.err.println("Médico já cadastrado");
    		}
    	} else {
    		System.err.println("Campos obrigatórios não preenchidos");
    	}
    	
    }
    
    public static void alterarhospital(Hospital hospital) {
    	
    	if (verificar(hospital)) {
    		if (!HospitalDAO.isHospitalInserido(hospital.getCnpjHospital())) {
    			HospitalDAO.alterarHospital(hospital);
    		} else {
    			System.err.println("Médico já cadastrado");
    		}
    	} else {
    		System.err.println("Campos obrigatórios não preenchidos");
    	}
    	
    }
    
    public static Hospital carregarhospital(int idHospital) {
    	
    	if (idHospital > 0) {
    		return HospitalDAO.carregarHospital(idHospital);
    	} else {
    		return null;
    	}
    	
    }
    
    public static int carregarIdhospital(String cnpjHospital) {
    	if (cnpjHospital != null) {
    		return HospitalDAO.carregarIdHospital(cnpjHospital);
    	} else {
    		return 0;
    	}
    }

    public static void removerHospital(int idHospital) {
    	
    	if (idHospital > 0) {
    		HospitalDAO.removerHospital(idHospital);
    	}
    	
    }
    
    public static ArrayList<Hospital> ListarHospital() {
    	
        return HospitalDAO.listarHospital();
        
    }
    
}