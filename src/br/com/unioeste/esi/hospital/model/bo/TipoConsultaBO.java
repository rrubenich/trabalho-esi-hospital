package br.com.unioeste.esi.hospital.model.bo;

import br.com.unioeste.esi.hospital.model.vo.TipoConsulta;
import br.com.unioeste.esi.hospital.model.dao.TipoConsultaDAO;
import java.util.ArrayList;

public class TipoConsultaBO {
    
    TipoConsultaDAO tpDAO = new TipoConsultaDAO();
    
    private boolean Verify(TipoConsulta tp){
        return tp.getNomeTipoConsulta().length() > 5;
    }
    
    public boolean Create(TipoConsulta tp) {
        if(Verify(tp)){
            return tpDAO.Create(tp);
        }else{
            return false;
        }
    }

    public TipoConsulta Read(int idTipoConsulta) {
        if(idTipoConsulta > 0){   
            return tpDAO.Read(idTipoConsulta);
        }else{
            return null;
        }
    }

    public boolean Update(TipoConsulta tp) {
        if(Verify(tp)){
            return tpDAO.Update(tp);
        }else{
            return false;
        }
    }

    public boolean Delete(TipoConsulta tp) {
        if(tp.getIdTipoConsulta() > 0){
            return tpDAO.Delete(tp);
        }else{
            return false;
        }
    }

    public ArrayList<TipoConsulta> List() {
        return tpDAO.List();
    }
    
}
