package br.com.unioeste.esi.hospital.model.vo;

public class Hospital {

	private int idHospital;
	
	private int idMedicoResponsavelHospital;
	private String nomeHospital;
	private String cnpjHospital;
	
	private int idEnderecoHospital;
	private String complementoEnderecoHospital;
	private int numeroEnderecoHospital;
	
	public Hospital(int idHospital, int idMedicoResponsavelHospital, 
			String nomeHosptal, String cnpjHospital, int idEnderecoHospital,
			String complementoEnderecoHospital, int numeroEnderecoHospital) {
		this.idHospital = idHospital;
		this.nomeHospital = nomeHosptal;
		this.cnpjHospital = cnpjHospital;
		this.idEnderecoHospital = idEnderecoHospital;
		this.complementoEnderecoHospital = complementoEnderecoHospital;
		this.numeroEnderecoHospital = numeroEnderecoHospital;
		this.idMedicoResponsavelHospital = idMedicoResponsavelHospital;
	}
	
	public Hospital() {
	
	}

	public int getIdHospital() {
		return idHospital;
	}

	public void setIdHospital(int idHospital) {
		this.idHospital = idHospital;
	}

	public int getIdMedicoResponsavelHospital() {
		return idMedicoResponsavelHospital;
	}

	public void setIdMedicoResponsavelHospital(int idMedicoResponsavelHospital) {
		this.idMedicoResponsavelHospital = idMedicoResponsavelHospital;
	}

	public String getNomeHospital() {
		return nomeHospital;
	}

	public void setNomeHospital(String nomeHospital) {
		this.nomeHospital = nomeHospital;
	}

	public String getCnpjHospital() {
		return cnpjHospital;
	}

	public void setCnpjHospital(String cnpjHospital) {
		this.cnpjHospital = cnpjHospital;
	}

	public int getIdEnderecoHospital() {
		return idEnderecoHospital;
	}

	public void setIdEnderecoHospital(int idEnderecoHospital) {
		this.idEnderecoHospital = idEnderecoHospital;
	}

	public String getComplementoEnderecoHospital() {
		return complementoEnderecoHospital;
	}

	public void setComplementoEnderecoHospital(String complementoEnderecoHospital) {
		this.complementoEnderecoHospital = complementoEnderecoHospital;
	}

	public int getNumeroEnderecoHospital() {
		return numeroEnderecoHospital;
	}

	public void setNumeroEnderecoHospital(int numeroEnderecoHospital) {
		this.numeroEnderecoHospital = numeroEnderecoHospital;
	}
	
}
