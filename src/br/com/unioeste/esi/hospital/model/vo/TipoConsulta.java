package br.com.unioeste.esi.hospital.model.vo;

public class TipoConsulta {
    private int idTipoConsulta;
    private String nomeTipoConsulta;

    public TipoConsulta(int idTipoConsulta, String nomeTipoConsulta) {
        this.idTipoConsulta = idTipoConsulta;
        this.nomeTipoConsulta = nomeTipoConsulta;
    }

    public int getIdTipoConsulta() {
        return idTipoConsulta;
    }

    public void setIdTipoConsulta(int idTipoConsulta) {
        this.idTipoConsulta = idTipoConsulta;
    }

    public String getNomeTipoConsulta() {
        return nomeTipoConsulta;
    }

    public void setNomeTipoConsulta(String nomeTipoConsulta) {
        this.nomeTipoConsulta = nomeTipoConsulta;
    }
}
