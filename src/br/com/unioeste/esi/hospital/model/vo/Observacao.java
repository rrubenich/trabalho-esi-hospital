package br.com.unioeste.esi.hospital.model.vo;

public class Observacao {
    private int idObservacao;
    private int idConsulta;
    private String tituloObservacao;
    private String observacao;

    public Observacao(int idObservacao, int idConsulta, String tituloObservacao, String observacao) {
        this.idObservacao = idObservacao;
        this.idConsulta = idConsulta;
        this.tituloObservacao = tituloObservacao;
        this.observacao = observacao;
    }

    public int getIdObservacao() {
        return idObservacao;
    }

    public void setIdObservacao(int idObservacao) {
        this.idObservacao = idObservacao;
    }

    public int getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(int idConsulta) {
        this.idConsulta = idConsulta;
    }

    public String getTituloObservacao() {
        return tituloObservacao;
    }

    public void setTituloObservacao(String tituloObservacao) {
        this.tituloObservacao = tituloObservacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
