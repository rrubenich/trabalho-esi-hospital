package br.com.unioeste.esi.hospital.model.vo;

public class Medico {
	
	private int idMedico;
	
	private String nome;
	private String cpf;
	private String crm;
	
	private int idEndereco;
	private String complemento;
	private int numeroEndereco;
	
	//Construtor
	public Medico(int idMedico,String nome, String cpf, String crm, 
			int idEndereco, String complemento, int numeroEndereco) {
		this.idMedico = idMedico;
		this.nome = nome;
		this.cpf = cpf;
		this.crm = crm;
		this.idEndereco = idEndereco;
		this.complemento = complemento;
		this.numeroEndereco = numeroEndereco;
	}
	
	public Medico() {
		
	}
	
	//Get e Set
	public int getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(int idMedico) {
		this.idMedico = idMedico;
	}

	public int getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getCrm() {
		return crm;
	}
	
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public int getNumeroEndereco() {
		return numeroEndereco;
	}
	
	public void setNumeroEndereco(int numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

}
