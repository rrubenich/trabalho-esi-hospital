package br.com.unioeste.esi.hospital.model.vo;

import java.sql.Date;
import java.sql.Time;

public class Consulta {
    private int idConsulta;
    private int idMedico;
    private int idPaciente;
    private int idTipoConsulta;
    private int idHospital;
    private java.sql.Date dataConsulta;
    private java.sql.Time horarioConsulta;

    public Consulta(int idConsulta, int idMedico, int idPaciente, int idTipoConsulta, int idHospital, Date dataConsulta, Time horarioConsulta) {
        this.idConsulta = idConsulta;
        this.idMedico = idMedico;
        this.idPaciente = idPaciente;
        this.idTipoConsulta = idTipoConsulta;
        this.idHospital = idHospital;
        this.dataConsulta = dataConsulta;
        this.horarioConsulta = horarioConsulta;
    }

    public int getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(int idConsulta) {
        this.idConsulta = idConsulta;
    }

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getIdTipoConsulta() {
        return idTipoConsulta;
    }

    public void setIdTipoConsulta(int idTipoConsulta) {
        this.idTipoConsulta = idTipoConsulta;
    }

    public int getIdHospital() {
        return idHospital;
    }

    public void setIdHospital(int idHospital) {
        this.idHospital = idHospital;
    }

    public Date getDataConsulta() {
        return dataConsulta;
    }

    public void setDataConsulta(Date dataConsulta) {
        this.dataConsulta = dataConsulta;
    }

    public Time getHorarioConsulta() {
        return horarioConsulta;
    }

    public void setHorarioConsulta(Time horarioConsulta) {
        this.horarioConsulta = horarioConsulta;
    }
}
