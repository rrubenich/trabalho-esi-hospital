package br.com.unioeste.esi.hospital.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.unioeste.esi.hospital.model.vo.Hospital;
import conexao.Conexao;

public class HospitalDAO {

	public static void inserirHospital(Hospital hospital) {

		try {
			PreparedStatement stat;
			stat=Conexao.con.prepareStatement("INSERT INTO Hospital(idMedicoResponsavelHospital, nomeHospital, cnpjHospital, "
					+ "idEnderecoHospital, complementoEnderecoHospital, numeroEnderecoHospital) VALUES (?, ?, ?, ?, ?, ?)");
			stat.setInt(1, hospital.getIdMedicoResponsavelHospital());
			stat.setString(2, hospital.getNomeHospital());
			stat.setString(3, hospital.getCnpjHospital());
			stat.setInt(4, hospital.getIdEnderecoHospital());
			stat.setString(5, hospital.getComplementoEnderecoHospital());
			stat.setInt(6, hospital.getNumeroEnderecoHospital());
			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Gravado com Sucesso!");
			} else {
				System.err.println("Não foi possível realizar a gravação!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void alterarHospital(Hospital hospital) {

		try {
			PreparedStatement stat;

			stat=Conexao.con.prepareStatement("UPDATE Hospital SET "
					+ "idMedicoResponsavelHospital = " + hospital.getIdMedicoResponsavelHospital() + ","
					+ "nomeHospital = '" + hospital.getNomeHospital() + "',"
					+ "cnpjHospital = '" + hospital.getCnpjHospital() + "',"
					+ "idEnderecoHospital = " + hospital.getIdEnderecoHospital() + ","
					+ "complementoEnderecoHospital = '" + hospital.getComplementoEnderecoHospital() + "',"
					+ "numeroEnderecoHospital = '" + hospital.getNumeroEnderecoHospital() + "'"
					+ "WHERE idHospital = " + hospital.getIdHospital() + ";");

			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Editado com sucesso!");
			} else {
				System.err.println("Não foi possível realizar a edição!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void removerHospital(int idHospital) {

		try {
			PreparedStatement stat;
			stat=Conexao.con.prepareStatement("DELETE FROM Hospital WHERE idHospital = '" + idHospital + "'");
			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Removido com Sucesso!");
			} else {
				System.err.println("Não Foi Possível Realizar a Remoção!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Hospital carregarHospital(int idHospital) {

		ResultSet rs;

		try {
			rs = Conexao.stm.executeQuery("SELECT * FROM Hospital WHERE idHospital = " + idHospital);
			rs.last();

			Hospital hospital = new Hospital(
					rs.getInt("idHospital"),
					rs.getInt("idMedicoResponsavelHospital"),
					rs.getString("nomeHospital"),
					rs.getString("cnpjHospital"),
					rs.getInt("idEnderecoHospital"),
					rs.getString("complementoEnderecoHospital"),
					rs.getInt("numeroEnderecoHospital"));

			return hospital;
		} catch (SQLException e) {
			e.printStackTrace();

			return null;
		}

	}
	
	public static boolean isHospitalInserido(String cnpjHospital) {
		
		ResultSet rs;
		
		try {
			rs = Conexao.stm.executeQuery("SELECT * FROM Hospital WHERE cnpjHospital = '" + cnpjHospital + "';");
			
			if(rs.last()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			return false;
		}
	}
	

	public static ResultSet carregarTabelaHospital() {

	        ResultSet rs;

	        try {
	            rs = Conexao.stm.executeQuery("SELECT * FROM Hospital");

	            return rs;
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());

	            return null;
	        }

	}
	
	public static int carregarIdHospital(String cnpjHospital) {
		ResultSet rs;
		
		try {
			rs = Conexao.stm.executeQuery("SELECT idHospital FROM Hospital WHERE cnpjHospital = '" + cnpjHospital + "';");
			rs.next();
			return rs.getInt("idHospital");
			
		} catch (SQLException e) {
			e.printStackTrace();
			
			return 0;
		}
	}

	public static ArrayList<Hospital> listarHospital() {

        ArrayList<Hospital> lista = new ArrayList<Hospital>();

        ResultSet rs = carregarTabelaHospital();

        try {
            while (rs.next()) {
            	Hospital hospital = new Hospital(
    					rs.getInt("idHospital"),
    					rs.getInt("idMedicoResponsavelHospital"),
    					rs.getString("nomeHospital"),
    					rs.getString("cnpjHospital"),
    					rs.getInt("idEnderecoHospital"),
    					rs.getString("complementoEnderecoHospital"),
    					rs.getInt("numeroEnderecoHospital"));

                lista.add(hospital);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

            return null;
        }

        return lista;
    }

}