package br.com.unioeste.esi.hospital.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.unioeste.esi.hospital.control.Conexao;
import br.com.unioeste.esi.hospital.model.vo.Observacao;

public class ObservacaoDAO {

    String query;

    public boolean Create(Observacao o) {

        query = "INSERT INTO Observacao (idConsulta, tituloObservacao, observacao) VALUES ("
                + o.getIdConsulta() + ", '"
                + o.getTituloObservacao() + "', '"
                + o.getObservacao() + "')";

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public Observacao Read(int idObservacao) {
        ResultSet rs = null;

        query = "SELECT * FROM Observacao WHERE idObservacao = " + idObservacao;

        try {
            rs = Conexao.stm.executeQuery(query);

            rs.last();

            Observacao o = new Observacao(
                    rs.getInt("idObservacao"),
                    rs.getInt("idConsulta"),
                    rs.getString("tituloObservacao"),
                    rs.getString("observacao"));

            return o;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public ResultSet ReadAll() {

        ResultSet rs = null;

        try {
            rs = Conexao.stm.executeQuery("SELECT * FROM Observacao");

            return rs;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }

    }

    public boolean Update(Observacao o) {

        query = "UPDATE Observacao SET "
                + "idConsulta = " + o.getIdConsulta() + ", "
                + "tituloObservacao = '" + o.getTituloObservacao() + "', "
                + "observacao = '" + o.getObservacao() + "', "
                + "WHERE idConsulta = " + o.getIdObservacao();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean Delete(Observacao o) {
        query = "DELETE FROM Observacao WHERE idObservacao = " + o.getIdObservacao();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public ArrayList<Observacao> List() {
        ArrayList<Observacao> list = new ArrayList<Observacao>();

        ResultSet rs = ReadAll();

        try {
            while (rs.next()) {
                Observacao o = new Observacao(
                        rs.getInt("idObservacao"),
                        rs.getInt("idConsulta"),
                        rs.getString("tituloObservacao"),
                        rs.getString("observacao"));

                list.add(o);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }

        return list;
    }
}
