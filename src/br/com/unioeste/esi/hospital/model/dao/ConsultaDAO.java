package br.com.unioeste.esi.hospital.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.unioeste.esi.hospital.control.Conexao;
import br.com.unioeste.esi.hospital.model.vo.Consulta;

public class ConsultaDAO {

    String query;

    public boolean Create(Consulta c) {

            query = "INSERT INTO Consulta (idMedico, idPaciente, idTipoConsulta, idHospital, dataConsulta, horarioConsulta) VALUES "
                    + "(" + c.getIdMedico() 
                    + ", " + c.getIdPaciente() 
                    + ", " + c.getIdTipoConsulta() 
                    + ", " + c.getIdHospital() 
                    + ", '" + c.getDataConsulta() 
                    + "', '" + c.getHorarioConsulta() + "')";
        

        try {
            Conexao.stm.executeUpdate(query);
            
            return true;
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public Consulta Read(int idConsulta) {

        ResultSet rs = null;

        try {
            rs = Conexao.stm.executeQuery("SELECT * FROM Consulta WHERE idConsulta = " + idConsulta);

            rs.last();

            Consulta c = new Consulta(
                    rs.getInt("idConsulta"),
                    rs.getInt("idMedico"),
                    rs.getInt("idPaciente"),
                    rs.getInt("idTipoConsulta"),
                    rs.getInt("idHospital"),
                    rs.getDate("dataConsulta"),
                    rs.getTime("horarioConsulta"));

            return c;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

            return null;
        }

    }
    
    public Consulta Read(int idMedico, java.sql.Date data, java.sql.Time horario) {

        ResultSet rs = null;

        try {
            rs = Conexao.stm.executeQuery("SELECT * FROM Consulta WHERE idMedico = " + idMedico + " AND dataConsulta = '" + data + "' AND horarioConsulta = '" + horario + "'");
            
            rs.last();

            Consulta c = new Consulta(
                    rs.getInt("idConsulta"),
                    rs.getInt("idMedico"),
                    rs.getInt("idPaciente"),
                    rs.getInt("idTipoConsulta"),
                    rs.getInt("idHospital"),
                    rs.getDate("dataConsulta"),
                    rs.getTime("horarioConsulta"));

            return c;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    public ResultSet ReadAll() {

        ResultSet rs = null;

        try {
            rs = Conexao.stm.executeQuery("SELECT * FROM Consulta");

            return rs;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    public boolean Update(Consulta c) {

        
            query = "UPDATE Consulta SET "
                    + "idMedico = " + c.getIdMedico() + ", "
                    + "idPaciente = " + c.getIdPaciente() + ", "
                    + "idTipoConsulta = " + c.getIdTipoConsulta() + ", "
                    + "idHospital = " + c.getIdHospital() + ", "
                    + "horarioConsulta = '" + c.getHorarioConsulta() + "', "
                    + "dataConsulta = '" + c.getDataConsulta() + "' "
                    + "WHERE idConsulta = " + c.getIdConsulta();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean Delete(Consulta c) {
        query = "DELETE FROM Consulta WHERE idFatura = " + c.getIdConsulta();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public ArrayList<Consulta> List() {
        ArrayList<Consulta> list = new ArrayList<Consulta>();

        ResultSet rs = ReadAll();
        
        try {
            while (rs.next()) {
                Consulta c = new Consulta(
                        rs.getInt("idConsulta"),
                        rs.getInt("idMedico"),
                        rs.getInt("idPaciente"),
                        rs.getInt("idTipoConsulta"),
                        rs.getInt("idHospital"),
                        rs.getDate("dataConsulta"),
                        rs.getTime("horarioConsulta"));

                list.add(c);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

        return list;
    }
    
}
