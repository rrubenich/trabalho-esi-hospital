package br.com.unioeste.esi.hospital.model.dao;

import br.com.unioeste.esi.hospital.model.vo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import conexao.Conexao;

public class MedicoDAO {
		
	public static void inserirMedico(Medico medico) {
		
		try {
			PreparedStatement stat;
			stat = Conexao.con.prepareStatement("INSERT INTO Medico(nomeMedico, cpfMedico, crmMedico, "
					+ "idEndereco, complementoEnderecoMedico, numeroEnderecoMedico) VALUES (?, ?, ?, ?, ?, ?)");
			stat.setString(1, medico.getNome());
			stat.setString(2, medico.getCpf());
			stat.setString(3, medico.getCrm());
			stat.setInt(4, medico.getIdEndereco());
			stat.setString(5, medico.getComplemento());
			stat.setInt(6, medico.getNumeroEndereco());
			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Gravado com Sucesso!");
			} else {
				System.err.println("Não foi possível realizar a gravação!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static boolean isMedicoInserido(String cpfMedico) {
		
		ResultSet rs;
		
		try {
			rs = Conexao.stm.executeQuery("SELECT * FROM Medico WHERE cpfMedico = '" + cpfMedico + "';");
			
			if(rs.last()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
	public static void alterarMedico(Medico medico) {
		
		try {
			PreparedStatement stat;
			
			stat=Conexao.con.prepareStatement("UPDATE Medico SET "
					+ "nomeMedico = '" + medico.getNome() + "',"
					+ "cpfMedico = '" + medico.getCpf() + "',"
					+ "crmMedico = '" + medico.getNome() + "',"
					+ "idEndereco = " + medico.getIdEndereco() + ","
					+ "complementoEnderecoMedico = '" + medico.getComplemento() + "',"
					+ "numeroEnderecoMedico = '" + medico.getNumeroEndereco() + "'"
					+ "WHERE idMedico = " + medico.getIdMedico() + ";");
			
			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Editado com sucesso!");
			} else {
				System.err.println("Não foi possível realizar a edição!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void removerMedico(int idMedico) {
		
		try {
			PreparedStatement stat;
			stat=Conexao.con.prepareStatement("DELETE FROM Medico WHERE idMedico = '" + idMedico + "'");
			int count = stat.executeUpdate();
			if (count > 0) {
				System.out.println("Removido com Sucesso!");
			} else {
				System.err.println("Não Foi Possível Realizar a Remoção!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static Medico carregarMedico(int idMedico) {
		
		ResultSet rs;
		
		try {
			rs = Conexao.stm.executeQuery("SELECT * FROM Medico WHERE idMedico = " + idMedico);
			rs.last();
			
			Medico medico = new Medico(
					rs.getInt("idMedico"),
					rs.getString("nomeMedico"),
					rs.getString("cpfMedico"),
					rs.getString("crmMedico"),
					rs.getInt("idEndereco"),
					rs.getString("complementoEnderecoMedico"),
					rs.getInt("numeroEnderecoMedico"));
			
			return medico;
		} catch (SQLException e) {
			e.printStackTrace();
			
			return null;
		}
		
	}

	public static int carregarIdMedico(String cpfMedico) {
		ResultSet rs;
		
		try {
			rs = Conexao.stm.executeQuery("SELECT idMedico FROM Medico WHERE cpfMedico = '" + cpfMedico + "';");
			rs.next();
			return rs.getInt("idMedico");
			
		} catch (SQLException e) {
			e.printStackTrace();
			
			return 0;
		}
	}
	
	public static ResultSet carregarTabelaMedico() {

	        ResultSet rs;

	        try {
	            rs = Conexao.stm.executeQuery("SELECT * FROM Medico");

	            return rs;
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            
	            return null;
	        }
	        
	}
	
	public static ArrayList<Medico> listarMedico() {
		
        ArrayList<Medico> lista = new ArrayList<Medico>();

        ResultSet rs = carregarTabelaMedico();

        try {
            while (rs.next()) {
    			Medico medico = new Medico(
    					rs.getInt("idMedico"),
    					rs.getString("nomeMedico"),
    					rs.getString("cpfMedico"),
    					rs.getString("crmMedico"),
    					rs.getInt("idEndereco"),
    					rs.getString("complementoEnderecoMedico"),
    					rs.getInt("numeroEnderecoMedico"));

                lista.add(medico);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            
            return null;
        }

        return lista;
    }
	
}
		
