package br.com.unioeste.esi.hospital.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.com.unioeste.esi.hospital.control.Conexao;
import br.com.unioeste.esi.hospital.model.vo.TipoConsulta;

public class TipoConsultaDAO {

    String query;

    public boolean Create(TipoConsulta tp) {
        query = "INSERT INTO TipoConsulta (nomeTipoConsulta) VALUES ('"
                + tp.getNomeTipoConsulta() + "')";

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public TipoConsulta Read(int idTipoConsulta) {
        ResultSet rs = null;

        query = "SELECT * FROM TipoConsulta WHERE idTipoConsulta = " + idTipoConsulta;

        try {
            rs = Conexao.stm.executeQuery(query);

            rs.last();

            TipoConsulta tp = new TipoConsulta(
                    rs.getInt("idTipoConsulta"),
                    rs.getString("nomeTipoConsulta"));

            return tp;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public ResultSet ReadAll() {

        ResultSet rs = null;

        try {
            rs = Conexao.stm.executeQuery("SELECT * FROM TipoConsulta");

            return rs;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    public boolean Update(TipoConsulta tp) {
        query = "UPDATE TipoConsulta SET "
                + "nomeTipoConsulta = '" + tp.getNomeTipoConsulta() + "' "
                + "WHERE idTipoConsulta = " + tp.getIdTipoConsulta();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Delete(TipoConsulta tp) {
        query = "DELETE FROM TipoConsulta WHERE idTipoConsulta = " + tp.getIdTipoConsulta();

        try {
            Conexao.stm.executeUpdate(query);

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ArrayList<TipoConsulta> List() {
        ArrayList<TipoConsulta> list = new ArrayList<TipoConsulta>();

        ResultSet rs = ReadAll();

        try {
            while (rs.next()) {
                TipoConsulta tp = new TipoConsulta(
                        rs.getInt("idTipoConsulta"),
                        rs.getString("nomeTipoConsulta"));

                list.add(tp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

        return list;
    }
}
